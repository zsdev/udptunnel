/*
*zs zhangsong@adtsec.com
*QQ 84500316@qq.com
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/time.h>
#include <netinet/ether.h>
#include <net/ethernet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <list.h>
#include <uservices.h>
#include <errno.h>
#include <getopt.h>

struct client_info {
	struct list_head list;
	struct list_head hlist;
	unsigned long expired;
	unsigned int ip;
	unsigned short port;
	unsigned int vip;
};

enum {
	TUN_HANDSHAKE  =1,
	TUN_DATA,
	TUN_KEEPALIVE,
};
struct tunhdr {
	unsigned char type;
	unsigned int id;
	unsigned char data[0];
};

#define QUERY_TIMEOUT_LIST 30
#define QUERY_TIMEOUT 5
#define TUN_NAME "tun100"
#define TUN_MTU 1518
#define TUN_MAX 2048

static struct list_head g_client_list;
static struct list_head g_client_hlist[TUN_MAX];
static int g_server_mode = 0;
static int g_targetip;
static int g_targetport;
//从5.0.0.2开始分配IP
static int g_startip = 0x05000002;
static int g_alive_retries = 0;
static int g_debug = 0;

void D(const char *fmt, ...) {

	if (!g_debug)
		return;
	va_list arglist;
	va_start(arglist, fmt);
	vprintf(fmt, arglist);
	va_end(arglist);
}

static void setup_link(const char *dev, unsigned int ip) 	/* Set link up */
{
	char script_buf[128] = {0};
	sprintf(script_buf,
			"ifconfig %s %d.%d.%d.%d netmask 255.0.0.0 up",
			dev, HIPQUAD(ip));
	//signal(SIGCHLD, SIG_DFL);
	system(script_buf);
	sleep(1);
}

int tun_open (const char *dev, int flags) {
	struct ifreq ifr;
	int fd;
	char *device = "/dev/net/tun";

	if ((fd = open (device, O_RDWR)) < 0) {
		printf("Cannot open TUN/TAP dev %s", device);
		exit(-1);
	}
	memset (&ifr, 0, sizeof (ifr));
	ifr.ifr_flags = flags;
	if (!strncmp (dev, "tun", 3)) {
		ifr.ifr_flags |= IFF_TUN;
	} else {
		printf("I don't recognize device %s as a TUN device",dev);
		exit(-1);
	}
	strncpy (ifr.ifr_name, dev, IFNAMSIZ);
	if (ioctl (fd, TUNSETIFF, (void *)&ifr) < 0) {
		printf( "Cannot ioctl TUNSETIFF %s", dev);
		exit(-1);
	}
	return fd;
}

static int init_socket(int fd) {
	int opt = 1;
	struct timeval to = {QUERY_TIMEOUT, 0};

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0) {
		printf("cannot create AAA socket %s\n", strerror(errno));
		exit(-1);
	}
	if (g_server_mode)
		setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO,(const void *)&to, sizeof(to)) == -1 ||
			setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO,(const void *)&to, sizeof(to)) == -1) {
		printf("AAA: setsockopt failed %s\n", strerror(errno));
		exit(-1);
	}
	return fd;
}

static inline unsigned long get_hash(unsigned int ip, unsigned short port) {
	return (jhash_3words(ip, port, 0, TUN_MAX)%TUN_MAX);
}

static void ev_cls(void) {
	struct client_info *c0, *c1;
	long now = get_seconds();

	list_for_each_entry_safe_reverse(c0, c1, &g_client_list, list) {
		unsigned long expired = c0->expired;
		if (now - (long)(expired + QUERY_TIMEOUT_LIST) > 0 || !expired) {
			list_del(&c0->list);
			list_del(&c0->hlist);
			free(c0);
			D("Free: %d.%d.%d.%d:%d\n", NIPQUAD(c0->ip), htons(c0->port));
			continue;
		}
	}
}

static void *ev_find(unsigned int ip, unsigned short port, unsigned int vip) {
	struct client_info *ep, *tmp;
	int hash = get_hash(vip, 0);

	list_for_each_entry_safe(ep, tmp, &g_client_hlist[hash], hlist) {
		if ((ip && port && ep->ip == ip && ep->port == port) ||
				(vip && vip == ep->vip)) {
			ep->expired = get_seconds();
			return ep;
		}
	}
	return NULL;
}

static void ev_add(unsigned int ip, unsigned short port, unsigned int vip) {
	struct client_info *ep;
	int hash = get_hash(vip, 0);

	if ((ep = ev_find(ip, port, vip)))
		return;
	ep = malloc(sizeof(*ep));
	if (!ep)
		return;
	memset(ep, 0, sizeof(*ep));
	ep->expired = get_seconds();
	ep->ip = ip;
	ep->port = port;
	ep->vip = vip;
	list_add(&ep->list, &g_client_list);
	list_add(&ep->hlist, &g_client_hlist[hash]);
	D("Alloc: %d.%d.%d.%d:%d\n", NIPQUAD(ep->ip), htons(ep->port));
}

static void udp_input(int fd, int devfd) {
	char data[TUN_MTU];
	int data_len;
	struct tunhdr *hdr;
	struct sockaddr_in addr;
	int addrlen = sizeof(addr);

	if (!g_server_mode)
		data_len = recv(fd, data, sizeof(data), 0);
	else
		data_len = recvfrom(fd, data, sizeof(data), 0, (struct sockaddr *)&addr, (socklen_t *)&addrlen);
	if (data_len <= 0) {
		if ((data_len == 0 && errno == EINTR) || errno == EAGAIN)
			return;
		exit(-1);
	}
	hdr  = (void *)data;
	if (g_server_mode) {
		if (hdr->type == TUN_DATA ||
				hdr->type == TUN_KEEPALIVE) {
			if (!ev_find(addr.sin_addr.s_addr, addr.sin_port, hdr->id))
				return;
		}
	}
	switch(hdr->type) {
		case TUN_DATA:
			if (0 > write(devfd, hdr->data, data_len - sizeof(*hdr)))
				printf("Bad tun-write %s\n", strerror(errno));
			break;
		case TUN_KEEPALIVE:
			if (g_server_mode) {
				sendto(fd, data, data_len, 0, (struct sockaddr *)&addr, sizeof(addr));
				D("Keepalive: %d.%d.%d.%d:%d\n",
						NIPQUAD(addr.sin_addr.s_addr), htons(addr.sin_port));
			} else {
				g_alive_retries = 0;
			}
			break;
		case TUN_HANDSHAKE:
			D("Handshake: %d.%d.%d.%d:%d\n",
					NIPQUAD(addr.sin_addr.s_addr), htons(addr.sin_port));
			if (g_server_mode) {
				*((unsigned int *)hdr->data) = htonl(g_startip);
				ev_add(addr.sin_addr.s_addr, addr.sin_port, htonl(g_startip));
				++g_startip;
				sendto(fd, data, sizeof(*hdr) + 4, 0,  (struct sockaddr *)&addr, sizeof(addr));
			}
			break;
		default:;
	}
}

static void dev_input(int fd, int ufd) {
	char data[TUN_MTU];
	int data_len;
	struct tunhdr *hdr;

	hdr = (void *)data;
	data_len = read(fd, hdr->data, TUN_MTU - sizeof(*hdr));
	if (data_len < 0) {
		printf("read tun error %d, %s\n", data_len, strerror(errno));
		exit(-1);
	} else if (data_len == 0)
		return;

	hdr->type = TUN_DATA;
	//源地址作为ID
	hdr->id = *((unsigned int *)(hdr->data + 12));
	data_len += sizeof(*hdr);
	if (g_server_mode) {
		unsigned int vip = 0;
		struct sockaddr_in addr = {0};
		struct client_info *ep;
		vip = *((unsigned int *)(hdr->data + 16));
		ep = ev_find(0, 0, vip);
		if (ep) {
			addr.sin_family = AF_INET;
			addr.sin_addr.s_addr = ep->ip;
			addr.sin_port = ep->port;
			sendto(ufd, data, data_len, 0, (struct sockaddr *)&addr, sizeof(addr));
		}
	} else {
		send(ufd, data, data_len, 0);
	}
}

static void keepalived(int fd, unsigned int tun_ip) {
	char data[64] = {0};
	struct tunhdr *hdr;

	hdr  = (void *)data;
	hdr->type = TUN_KEEPALIVE;
	hdr->id = htonl(tun_ip);
	send(fd, data, sizeof(*hdr) + 1, 0);
	g_alive_retries++;
	if (g_alive_retries > 5) {
		printf("Target unreacheable\n");
		exit(-1);
	}
}

static void loop(void) {
	struct sockaddr_in addr;
	int fd, devfd;
	int nev;
	unsigned int tun_ip = g_startip - 1;
	fd_set rfds;
	struct timeval tv = {0};

	fd = init_socket(0);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(g_targetport);
	addr.sin_addr.s_addr = htonl(g_targetip);
	if (g_server_mode) {
		if (-1 == bind(fd, (struct sockaddr *)&addr, sizeof(addr))) {
			printf("bind  failed %s\n", strerror(errno));
			exit(-1);
		}
	} else {
		struct tunhdr *hdr;
		char data[64];
		int datalen;

		if (-1 == connect(fd, (struct sockaddr *)&addr, sizeof(addr))) {
			printf("connect-bind  failed %s\n", strerror(errno));
			exit(-1);
		}
		hdr = (void *)data;
		hdr->type = TUN_HANDSHAKE;
		send(fd, data, sizeof(*hdr), 0);
		datalen = recv(fd, data, sizeof(data), 0);
		if (datalen <= 0) {
			printf("No server response %s\n", strerror(errno));
			exit(-1);
		}
		hdr = (void *)data;
		if (hdr->type != TUN_HANDSHAKE) {
			printf("Bad server response %s\n", strerror(errno));
			exit(-1);
		}
		tun_ip = *((unsigned int *)hdr->data);
		tun_ip = ntohl(tun_ip);
	}
	devfd = tun_open(TUN_NAME, IFF_TUN|IFF_NO_PI);
	setup_link(TUN_NAME, tun_ip);
	//Start loop
	FD_ZERO(&rfds);
	while (1) {
		FD_SET(fd, &rfds);
		FD_SET(devfd, &rfds);
		tv.tv_sec = QUERY_TIMEOUT;
		nev = select(fd>devfd?(fd+1):(devfd+1), &rfds, NULL, NULL, &tv);
		if (nev <= 0) {
			if (nev == -1 && errno != EINTR)
				exit(-1);
			if (g_server_mode)
				ev_cls();
			else
				keepalived(fd, tun_ip);
			continue;
		}

		if (FD_ISSET(devfd, &rfds)) {
			dev_input(devfd, fd);
		}
		if (FD_ISSET(fd, &rfds)) {
			udp_input(fd, devfd);
		}
		if (g_server_mode)
			ev_cls();
	}
	//Cannot reach
	exit(-1);
}

static struct option long_options[] = {
	{ "server", 0, NULL, 's' }, //rebuild database if not exist
	{ "client", 0, NULL, 'c' }, //rebuild database if not exist
	{ 0, 0, 0, 0},
};

static void parse_options(int argc, char *argv[]) {
	int c;
	struct in_addr addr;

	while((c = getopt_long (argc, argv, "dscp:t:", long_options, NULL)) != -1) {
		switch (c) {
			case 's':
				g_server_mode = 1;
				break;
			case 'd':
				g_debug = 1;
				break;
			case 'c':
				g_server_mode = 0;
				break;
			case 't':
				inet_aton(optarg, &addr);
				g_targetip = ntohl(addr.s_addr);
				break;
			case 'p':
				g_targetport=  strtoul(optarg, NULL, 0);
				break;
			default:
				printf("Bad option %c", c);
				exit(-1);
				break;
		}
	}
	return;
}

#ifdef AC_POINT
#include <json-c/json.h>
static void init_cfg(void) {
	FILE *fp = NULL;
	char cfg[CFG_BUF_SIZE];
	json_object *obj = NULL, *n;
	int isexist = 0;
	struct in_addr addr;

	fp = fopen(CFG_PATH"/"FJ_CFG, "r");
	if (!fp)
		goto err;
	if (!fgets(cfg, sizeof(cfg), fp))
		goto err;
	obj = json_tokener_parse(cfg);
	if (!obj)
		goto err;
	isexist = json_object_object_get_ex(obj, "cloudip", &n);
	if (!isexist)
		goto err;
	 inet_aton(json_object_get_string(n), &addr);
	 g_targetip = ntohl(addr.s_addr);
	isexist = json_object_object_get_ex(obj, "cloudport", &n);
	if (!isexist)
		goto err;
	g_targetport =  strtoul(json_object_get_string(n), NULL, 0);
out:
	if (fp)
		fclose(fp);
	if (obj)
		json_object_put(obj);
	return;
err:
	goto out;
}
#else
static void init_cfg(void) {
	return;
}
#endif

static void loop2(void) {
	while (1) {
		select(1, NULL, NULL, NULL, NULL);
	}
}

int main(int argc, char *argv[]) {
	int i;

	init_cfg();
	parse_options(argc, argv);
	if (!g_targetip || !g_targetport) {
		if (!g_server_mode)
			loop2();
	}
	list_init_head(&g_client_list);
	for (i = 0; i < TUN_MAX; i++)
		list_init_head(&g_client_hlist[i]);
	loop();
	return 0;
}
