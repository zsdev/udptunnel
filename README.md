# udptunnel.c
建立分支于中心的UDP隧道用于任意结点管理

1.编译 gcc -o udptunnel udptunnel.c -I .

2.服务端运行
./udptunnel -s -p ServerPort

3.网络终端运行
./udptunnel -c -t ServerIP -p ServerPort

4.测试
在服务端
ping 5.0.0.2


# proxystack.c
通过代理服务器隧道（tcp）

本地->本地代理->远程代理->目标代理

 gcc -o vpntun proxystack.c -I . -g
 
./vpntun  -d -x 120.27.99.77 -p 3129 -x.x.x.x -P xx -k key-string -l xxx

添加iptables规则引流量到本地

sudo iptables -t nat -A OUTPUT -p tcp --dport 80 -j REDIRECT --to-ports 8800


# android部署

export PATH=/home/ak47/android-ndk-r10b:$PATH

tunnel/

├── jni

├── jni/xxx.c

//build

ndk-build

