#ifndef USERVICES_H
#define USERVICES_H

#include <sys/time.h>
#include <stdint.h>

//
/* Porting hash alg from linux kernel */
#define __jhash_mix(a, b, c) \
{ \
	a -= b; a -= c; a ^= (c>>13); \
	b -= c; b -= a; b ^= (a<<8); \
	c -= a; c -= b; c ^= (b>>13); \
	a -= b; a -= c; a ^= (c>>12);  \
	b -= c; b -= a; b ^= (a<<16); \
	c -= a; c -= b; c ^= (b>>5); \
	a -= b; a -= c; a ^= (c>>3);  \
	b -= c; b -= a; b ^= (a<<10); \
	c -= a; c -= b; c ^= (b>>15); \
}

/* The golden ration: an arbitrary value */
#define JHASH_GOLDEN_RATIO	0x9e3779b9


/* A special ultra-optimized versions that knows they are hashing exactly
 *  * 3, 2 or 1 word(s).
 *   *
 *    * NOTE: In partilar the "c += length; __jhash_mix(a,b,c);" normally
 *     *       done at the end is not done here.
 *      */
static inline unsigned long jhash_3words(unsigned long a, unsigned long b, unsigned long c, unsigned long initval)
{
	a += JHASH_GOLDEN_RATIO;
	b += JHASH_GOLDEN_RATIO;
	c += initval;

	__jhash_mix(a, b, c);

	return c;
}

static inline unsigned long get_seconds(void) {
	struct timeval tv_s;
	gettimeofday(&tv_s, NULL);
	return tv_s.tv_sec;
}

#define NIPQUAD(addr) \
	((unsigned char *)&addr)[0], \
((unsigned char *)&addr)[1], \
((unsigned char *)&addr)[2], \
((unsigned char *)&addr)[3]

#define HIPQUAD(addr) \
	((unsigned char *)&addr)[3], \
((unsigned char *)&addr)[2], \
((unsigned char *)&addr)[1], \
((unsigned char *)&addr)[0]


#endif
