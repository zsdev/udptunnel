/*
 *QQ 84500316@qq.com
 */
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/time.h>
#include <netinet/ether.h>
#include <net/ethernet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <list.h>
#include <uservices.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <limits.h>
#include <time.h>
#include <linux/netfilter_ipv4.h>
#include <netinet/tcp.h>

static struct {
	uint32_t proxy_ip;
	uint16_t proxy_port;
	uint32_t real_serverip;
	uint16_t real_serverport;
	uint16_t local_port;
	uint32_t startip;
	uint8_t alive_retries;
	uint32_t allocipsource;
	int devfd;
	char *proxy_token;
	uint8_t debug:2;
	uint8_t servermode:1;
	uint8_t noserver:1;
}g_conf;
struct flow_info {
	struct list_head list;
	struct list_head hlist;
	struct list_head idlist;
	unsigned long expired;
	struct {
		uint32_t dstip;
		uint16_t dport;
		uint32_t id;
		int ffd;
	}i;
	int fd;
	uint32_t dyn:1;
	uint32_t origin:1;
	uint32_t connected:1;
	uint32_t tun2fd:1;
	uint32_t id_set:1;
	uint32_t fd2tun:1;
	uint32_t nego:1;
};
#pragma pack(1)
struct flow_hdr {
	uint8_t type;
	uint32_t tip;
	uint16_t tport;
	char data[0];
};
#pragma pack(0)
enum {
	TUN_HANDSHAKE  =1,
};
#define MAX_QUERY_NUM 32
#define MAX_FLOWIS 8046
#define FLOWI_TIMEOUT 60
#define VERSION "3.0"
#define TUN_NAME "tun100"
static struct list_head g_flowi_list;
static struct list_head g_flowi_hlist[MAX_FLOWIS];

void D(const char *fmt, ...) {

	if (!g_conf.debug)
		return;
	va_list arglist;
	va_start(arglist, fmt);
	vprintf(fmt, arglist);
	va_end(arglist);
}

static inline unsigned long get_hash(unsigned int ip, unsigned short port) {
	return (jhash_3words(ip, port, 0, 0)%MAX_FLOWIS);
}

static struct option long_options[] = {
	{ "proxy-ip", 1, NULL, 'X' }, //rebuild database if not exist
	{ "proxy-port", 1, NULL, 'P' }, //rebuild database if not exist
	{ "real-serverip", 1, NULL, 'x' }, //rebuild database if not exist
	{ "real-serverport", 1, NULL, 'p' }, //rebuild database if not exist
	{ "token", 1, NULL, 'k' }, //rebuild database if not exist
	{ "server-mode", 0, NULL, 's' }, //rebuild database if not exist
	{ 0, 0, 0, 0},
};

static void parse_options(int argc, char *argv[]) {
	int c;
	struct in_addr addr;

	while((c = getopt_long (argc, argv, "X:P:x:p:k:dl:sn", long_options, NULL)) != -1) {
		switch (c) {
			case 'd':
				g_conf.debug = 1;
				break;
			case 'X':
				inet_aton(optarg, &addr);
				g_conf.proxy_ip = ntohl(addr.s_addr);
				break;
			case 'P':
				g_conf.proxy_port=  strtoul(optarg, NULL, 0);
				break;
			case 'x':
				inet_aton(optarg, &addr);
				g_conf.real_serverip = ntohl(addr.s_addr);
				break;
			case 'p':
				g_conf.real_serverport=  strtoul(optarg, NULL, 0);
				break;
			case 'k':
				g_conf.proxy_token = strdup(optarg);
				break;
			case 'l':
				g_conf.local_port=  strtoul(optarg, NULL, 0);
				break;
			case 's':
				g_conf.servermode=  1;
				break;
			case 'n':
				g_conf.noserver=  1;
				break;
			default:
				printf("Bad option %c", c);
				exit(-1);
				break;
		}
	}
	g_conf.startip = 0x05000001;
	if (g_conf.servermode)
		g_conf.allocipsource = htonl(getpid());
	return;
}
//
int tun_open (const char *dev, int flags) {
	struct ifreq ifr;
	int fd;
	char *device = "/dev/tun";

	if ((fd = open (device, O_RDWR)) < 0) {
		printf("Cannot open TUN/TAP dev %s", device);
		exit(-1);
	}
	memset (&ifr, 0, sizeof (ifr));
	ifr.ifr_flags = flags;
	if (!strncmp (dev, "tun", 3)) {
		ifr.ifr_flags |= IFF_TUN;
	} else {
		printf("I don't recognize device %s as a TUN device",dev);
		exit(-1);
	}
	strncpy (ifr.ifr_name, dev, IFNAMSIZ);
	if (ioctl (fd, TUNSETIFF, (void *)&ifr) < 0) {
		printf( "Cannot ioctl TUNSETIFF %s", dev);
		exit(-1);
	}
	return fd;
}
static void setup_link(const char *dev, unsigned int ip) {
	char script_buf[128] = {0};
	sprintf(script_buf,
			"ifconfig %s %d.%d.%d.%d netmask 255.0.0.0;ip route rep default via 5.0.0.1 table 10",
			dev, HIPQUAD(ip));
	//signal(SIGCHLD, SIG_DFL);
	system(script_buf);
	sleep(1);
}
//
static int init_socket(int fd, int protocol, int create) {
	int opt = 1;
	struct timeval to = {5, 0};
	int skbufsz = 256*1024;

	if (create)
		fd = socket(AF_INET, protocol, 0);
	if (fd < 0) {
		printf("Oops: cannot create Socket %s\n", strerror(errno));
		exit(-1);
	}
	if (create)
		setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO,(const void *)&to, sizeof(to)) == -1 ||
			setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO,(const void *)&to, sizeof(to)) == -1 ||
			setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (const char*)&skbufsz, sizeof(int)) == -1 ||
			setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (const char*)&skbufsz, sizeof(int)) == -1 ||
			setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (void*)&opt, sizeof(opt))) {
		printf("Oops: Setsockopt failed %s\n", strerror(errno));
		exit(-1);
	}
	return fd;
}

//
static int create_epoll(struct epoll_event **pevlist) {
	struct epoll_event *evlist;
	int epfd;

	evlist = malloc(sizeof(*evlist) * (MAX_QUERY_NUM + 1));
	if (!evlist) {
		D("Oops: Alloc epoll event failed \n");
		exit(-1);
	}
	memset(evlist, 0, sizeof(*evlist) * (MAX_QUERY_NUM + 1));
	epfd = epoll_create(MAX_QUERY_NUM);
	if (epfd < 0) {
		D("Oops: create epoll IO %s\n", strerror(errno));
		exit(-1);
	}
	*pevlist = evlist;
	return epfd;
}
//
static void flowi_destroy(int epid, struct flow_info *flowi) {
	epoll_ctl(epid, EPOLL_CTL_DEL, flowi->fd, NULL);
	list_del(&flowi->list);
	list_del(&flowi->hlist);
	close(flowi->fd);
	if (flowi->id_set)
		list_del(&flowi->idlist);
	//	D("Free: %p, %d, %d\n", flowi, flowi->fd, flowi->i.ffd);
	free(flowi);
}
//
static void flowi_update(struct flow_info *flowi, int destroy) {
	if (flowi->dyn) {
		list_del(&flowi->list);
		if (destroy) {
			list_add(&flowi->list, &g_flowi_list);
			flowi->expired = 0;
		} else {
			list_add_tail(&flowi->list, &g_flowi_list);
			flowi->expired = get_seconds();;
		}
	}
}
//
static int flowi_sync(int ffd, int fd) {
	struct flow_info *c0, *c1;

	int hash = get_hash(fd, 0);
	list_for_each_entry_safe(c0, c1, &g_flowi_hlist[hash], hlist) {
		if (c0->fd == fd && c0->i.ffd == ffd) {
			c0->expired = 0;
			break;
		}
	}
	return 0;
}
//
static void flowi_cls(int epid, int fast) {
	struct flow_info *c0, *c1;
	long now = get_seconds();

	list_for_each_entry_safe(c0, c1, &g_flowi_list, list) {
		unsigned long expired = c0->expired;
		if (!c0->dyn)
			continue;
		if ( !expired || (now - (long)(expired + FLOWI_TIMEOUT) > 0)) {
			if (!c0->i.ffd ||
					0 == flowi_sync(c0->fd, c0->i.ffd))
				flowi_destroy(epid, c0);
			continue;
		}
		if (fast)
			break;
	}
}
//
static inline void* flowi_add(struct flow_info *flowi, int epid) {
	struct flow_info *nflowi;
	struct epoll_event ee = {0};
	nflowi = malloc(sizeof(*nflowi));
	if (!nflowi){
		D("Oops: Cannot alloc memory for flowi\n");\
			exit(-1);
	}
	memset(nflowi, 0, sizeof(*nflowi));
	*nflowi = *flowi;
	nflowi->expired = get_seconds();
	ee.data.ptr = (void *)nflowi;
	ee.events = EPOLLIN|EPOLLHUP;
	if (epoll_ctl(epid, EPOLL_CTL_ADD, flowi->fd, &ee) == -1) {
		D("Oops: Add event to epoll failed %s\n", strerror(errno));
		exit(-1);
	}
	if (nflowi->dyn) {
		int hash;
		hash = get_hash(flowi->fd, 0);
		list_add(&nflowi->hlist, &g_flowi_hlist[hash]);
		//timeout list
		list_add_tail(&nflowi->list, &g_flowi_list);
		//	D("Alloc: %p, %d, %d to %d.%d.%d.%d:%d\n",
		//			nflowi, nflowi->fd, nflowi->i.ffd,
		//		NIPQUAD(nflowi->i.dstip), htons(nflowi->i.dport));
	}
	return nflowi;
}
//
static int id_add(struct flow_info *flowi) {
	int hash = get_hash(flowi->i.id, 0);

	if (flowi->id_set) {
		D("BUG: mutiple idset\n");
		exit(-1);
	}
	list_add(&flowi->idlist, &g_flowi_hlist[hash]);
	flowi->id_set = 1;
	flowi_update(flowi, 0);
	return 0;
}
//
static void *id_find(uint32_t id) {
	struct flow_info *c0, *c1;

	int hash = get_hash(id, 0);
	list_for_each_entry_safe(c0, c1, &g_flowi_hlist[hash], idlist) {
		if (c0->id_set && c0->i.id == id)
			return c0;
	}
	return NULL;
}
//
int connectto(int port, unsigned int ip) {
	int fd;
	struct sockaddr_in	 addr;
	int flags, ret;
	struct timeval tv = {0};
	fd_set wset, rset;
	socklen_t len;

	//Disabled
	if (ip == 0)
		return -1;
	fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0) {
		printf("cannot create report socket %s\n", strerror(errno));
		return -1;
	}
	flags = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, flags|O_NONBLOCK);
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(ip);;
	ret = connect(fd, (struct sockaddr *)&addr, sizeof(struct sockaddr));
	if (ret < 0) {
		if (errno != EINPROGRESS)
			goto fin;
	} else
		goto done;
	FD_ZERO(&rset);
	FD_SET(fd,&rset);
	wset = rset;
	tv.tv_sec = 3;
	if ((ret = select(fd+1, &rset, &wset, NULL, &tv)) <= 0) {
		//EINTR ?
		goto fin;
	}
	if( FD_ISSET(fd, &rset) || FD_ISSET(fd, &wset) ) {
		len = sizeof(ret);
		/*
		 * BSDs and Linux return 0 and set a pending error in err
		 * Solaris returns -1 and sets errno
		 */
		if (getsockopt(fd, SOL_SOCKET, SO_ERROR, (void *) &ret, &len) == -1)
			goto fin;

		if (ret)
			goto fin;
done:
		flags = fcntl(fd, F_GETFL, 0)&(~O_NONBLOCK);
		fcntl(fd, F_SETFL, flags);
		init_socket(fd, SOCK_STREAM, 0);
		return fd;
	}
fin:
	close(fd);
	return -1;
}
//
static int connectproxy(void) {
	char data[256];
	int datalen;
	int ffd1;

	//非代理模式
	if (!g_conf.proxy_port || !g_conf.proxy_ip)
		return connectto(g_conf.real_serverport, g_conf.real_serverip);
	//如果proxy_xxx不是空说明使用代理模式，暂时支持HTTP-PROXY
	ffd1 = connectto(g_conf.proxy_port, g_conf.proxy_ip);
	if (ffd1 <= 0)
		return -1;
	snprintf(data, sizeof(data),
			"CONNECT %d.%d.%d.%d:%d HTTP/1.0\r\nHost: %d.%d.%d.%d\r\nProxy-Authorization: Basic %s\r\n\r\n",
			HIPQUAD(g_conf.real_serverip), g_conf.real_serverport, HIPQUAD(g_conf.real_serverip), g_conf.proxy_token);
	if (0 > send(ffd1, data, sizeof(data), 0)) {
		D("Warn: Send proxy request failed:%s\n", strerror(errno));
err:
		close(ffd1);
		return -1;
	}
	//D("Proxy request \n%s", data);
	memset(data, 0, sizeof(data));
	datalen = recv(ffd1, data, sizeof(data) - 1, 0);
	if (datalen <= 0) {
		D("Warn: No server response %s\n", strerror(errno));
		goto err;
	}
	//D("Proxy response %s\n", data);
	if (!strncmp(data, "HTTP/1.1 200", 12)) {
		//D("Connect proxy successfully\n");
	} else {
		D("Warn: Connect proxy failed \n%s\n", data);
		goto err;
	}
	return ffd1;
}
//
static int proxy_accept(int fd, int epid) {
	struct sockaddr_in addr = {0};
	int addrlen = sizeof(addr);
	int ffd, ffd1;
	struct flow_info flowi = {};

	ffd = accept(fd, (struct sockaddr *)&addr, (socklen_t *)&addrlen);
	if (ffd == -1) {
		//Too many files
		if (errno == EMFILE || errno == EINTR ||
				errno == EAGAIN || errno == ENFILE) {
			D("Warn: Accept %s\n", strerror(errno));
			return -1;
		}
		D("Oops:  Accept error %s\n", strerror(errno));
		exit(-1);
	}
	init_socket(ffd, SOCK_STREAM, 0);
	if (!g_conf.servermode) {
		ffd1 = connectproxy();
		if (ffd1 < 0) {
exp:
			close(ffd);
			return -1;
		}
		if(getsockopt(ffd, SOL_IP, SO_ORIGINAL_DST, &addr, (socklen_t *)&addrlen) == 0) {
			flowi.i.dport = addr.sin_port;
			flowi.i.dstip = addr.sin_addr.s_addr;
		} else {
			D("EXP: setsockopt %s\n", strerror(errno));
			goto exp;
		}
	}
	//Origin
	flowi.dyn = 1;
	flowi.origin = 1;
	flowi.i.ffd = ffd1;
	flowi.fd = ffd;
	flowi_add(&flowi, epid);
	if (!g_conf.servermode) {
		//Reply
		memset(&flowi, 0, sizeof(flowi));
		flowi.dyn = 1;
		flowi.i.ffd = ffd;
		flowi.fd = ffd1;
		flowi_add(&flowi, epid);
	}
	return 0;
}
//
static int mk_pairs(struct flow_info *flowi, char *data, int data_len, int epid, char *buf) {
	struct flow_hdr *fh;
	int ffd;
	struct flow_info rflowi = {};

	if (!g_conf.servermode)
		return 1;
	if (flowi->connected)
		return 1;
	if (data_len < sizeof(*fh)) {
		D("Warn: Recv small packet\n");
		goto err;
	}
	fh = (void *)data;
	flowi->connected = 1;
	if (fh->type == TUN_HANDSHAKE) {
		uint32_t allocsource = 0;
		if (data_len <  sizeof(*fh)) {
			D("Warn: too small handshake packet\n");
			goto err;
		}
		allocsource = *((unsigned int *)fh->data);
		if (allocsource == g_conf.allocipsource) {
			flowi->i.id = fh->tip;
			flowi->i.ffd = g_conf.devfd;
		} else {
			flowi->i.id = htonl(g_conf.startip + 1);
			flowi->i.ffd = g_conf.devfd;
			fh->tip = flowi->i.id;
			++g_conf.startip;
		}
		*((unsigned int *)fh->data) = g_conf.allocipsource;
		data_len = send(flowi->fd, data, sizeof(*fh) + 4, 0);
		if (data_len < 0)
			goto err;
		flowi->fd2tun  = 1;
		id_add(flowi);
		return 0;
	}
	ffd = connectto(ntohs(fh->tport), ntohl(fh->tip));
	if (ffd <= 0) {
		D("Warn: Connect server %d.%d.%d.%d:%d\n",
				NIPQUAD(fh->tip),ntohs(fh->tport));
		goto err;
	}
	//建立fd对
	flowi->i.ffd = ffd;
	//设置对端信息
	rflowi.dyn = 1;
	rflowi.i.ffd = flowi->fd;
	rflowi.fd = ffd;
	flowi_add(&rflowi, epid);
	return 1;
err:
	return -1;
}
//
static int build_all_tunnel(int init, int server, int epid) {
	struct flow_hdr *fh;
	int fd = 0, devfd;
	char data[64];
	int datalen;
	struct flow_info flowi = {};
	static uint32_t tunip = 0;
	static struct flow_info *flowi_dev = NULL;

	if (server) {
		tunip = g_conf.startip;
	} else {
		fd = connectproxy();
		fh = (void *)data;
		fh->type = TUN_HANDSHAKE;
		fh->tip = htonl(tunip);
		if (init)
			*((unsigned int *)fh->data) = 0;
		else
			*((unsigned int *)fh->data) = g_conf.allocipsource;
		send(fd, data, sizeof(*fh) + 4, 0);
		datalen = recv(fd, data, sizeof(data), 0);
		if (datalen <= 0) {
			D("Warn: No server response %s\n", strerror(errno));
			return -1;
		}
		fh = (void *)data;
		if (fh->type != TUN_HANDSHAKE) {
			D("Oops: Bad server response %s\n", strerror(errno));
			exit(-1);
		}
		tunip = ntohl(fh->tip);
		g_conf.allocipsource = *((unsigned int *)fh->data);
	}
	if (init) {
		devfd = tun_open(TUN_NAME, IFF_TUN|IFF_NO_PI);
		g_conf.devfd = devfd;
		//Origin
		if (!g_conf.servermode) {
			flowi.origin = 1;
			flowi.nego = 1;
		}
		flowi.i.ffd = fd;
		flowi.tun2fd = 1;
		flowi.fd = devfd;
		flowi_dev = flowi_add(&flowi, epid);
	}
	if (!server) {
		//Reply
		memset(&flowi, 0, sizeof(flowi));
		flowi.dyn = 1;
		flowi.i.ffd = g_conf.devfd;
		flowi.fd = fd;
		flowi.fd2tun = 1;
		flowi_add(&flowi, epid);
		//更新设备devfd对应的netfd
		flowi_dev->i.ffd = fd;
	}
	setup_link(TUN_NAME, tunip);
	D("Build tunnel successully\n");
	return fd;
}
static int proxy_forward(struct flow_info *flowi, int epid) {
	char buf[8192], *data;
	int data_len;
	struct flow_hdr *fh;

	//过期或被强制终止
	if (flowi->dyn && flowi->expired == 0) {
		D("Warn: expired flowi enter\n");
		goto err;
	}
	data = buf + sizeof(*fh);
	if (flowi->tun2fd) {
		data_len = read(flowi->fd, data, sizeof(buf) - sizeof(*fh));
		if (g_conf.servermode) {
			struct flow_info *rflowi;
			rflowi = id_find(*((unsigned int *)(data + 16)));
			if (!rflowi) {
				D("Warn not found rflow @%d.%d.%d.%d\n",
						NIPQUAD(*((unsigned int *)(data + 16))));
				return 0;
			}
			flowi->i.ffd = rflowi->fd;
		}
	} else
		data_len = recv(flowi->fd, data, sizeof(buf) - sizeof(*fh), 0);
	if (data_len <= 0) {
		if ((data_len == 0 && errno == EINTR) || errno == EAGAIN)
			return 0;
err:
		if (!g_conf.servermode && flowi->tun2fd)
			build_all_tunnel(0, 0, epid);
		flowi_update(flowi, 1);
		return -1;
	}
	//Reset flow header
	fh = (void *)data;
	if (flowi->origin) {
		switch(mk_pairs(flowi, data, data_len, epid, buf)) {
			case -1:
				goto err;
			case 0:
				flowi->nego = 1;
				return 0;
		}
		//发送前调整数据头部信息
		//情景1.客户端发送数据包
		if (!flowi->nego) {
			if (!g_conf.noserver && !g_conf.servermode) {
				data = buf;
				fh = (void *)data;
				if (flowi->tun2fd) {
					fh->tip = *((unsigned int *)(fh->data + 12));
				} else {
					fh->tip = flowi->i.dstip;
					fh->tport = flowi->i.dport;
				}
				data_len += sizeof(*fh);
			}//情景2. 发送到真实服务器前去掉私有数据头
			else if (g_conf.servermode) {
				data += sizeof(*fh);
				data_len -= sizeof(*fh);
			}
			//第一个数据包发送私有头部信息，
			//后续的数据包不需要在处理了
			flowi->nego = 1;
		}
	}
	if (flowi->fd2tun)
		data_len = write(flowi->i.ffd, data, data_len);
	else
		data_len = send(flowi->i.ffd, data, data_len, 0);
	if (data_len < 0) {
		if (flowi->fd2tun && errno == EINVAL) {
			D("Warn: Got fragment package\n");
		} else {
			D("Warn: send/write failed %s\n", strerror(errno));
			goto err;
		}
	}
	flowi_update(flowi, 0);
	return 0;
}
//
static void* loop(void) {
	struct sockaddr_in addr;
	int fd, epid;
	struct  epoll_event* evlist;
	int nev, i;
	struct flow_info *flowi, flow = {};
	int need_cls = 0;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(g_conf.local_port);
	addr.sin_addr.s_addr = INADDR_ANY;
	fd = init_socket(0, SOCK_STREAM, 1);
	if (-1 == bind(fd, (struct sockaddr *)&addr, sizeof(addr))) {
		printf("bind  failed %s\n", strerror(errno));
		exit(-1);
	}
	listen(fd, 511);
	flow.fd = fd;
	flow.dyn = 0;
	epid= create_epoll(&evlist);
	flowi_add(&flow, epid);
	if (build_all_tunnel(1, g_conf.servermode, epid) < 0) {
		printf("Oops: Cannot connect to server\n");
		exit(-1);
	}
	//Start loop
	while (1) {
		need_cls = 0;
		nev = epoll_wait(epid, evlist, MAX_QUERY_NUM, 1000*10);
		if (nev <= 0) {
			if (nev == -1 && errno != EINTR) {
				D("Oops epoll wait failed %d\n", errno);
				exit(-1);
			}
			flowi_cls(epid, 0);
			continue;
		}
		for (i = 0; i < nev; i++) {
			if (!evlist[i].events || !evlist[i].data.ptr) {
				D("Oops: Bad event\n");
				exit(-1);
			}
			flowi = (void *)evlist[i].data.ptr;
			if (flowi->fd == fd) {
				if (0 > proxy_accept(fd, epid))
					need_cls = 1;
			}  else {
				if (0 > proxy_forward(flowi, epid))
					need_cls = 1;
			}
		}
		if (need_cls)
			flowi_cls(epid, 1);
	}
	//Cannot reach
	D("Oops: bye\n");
	exit(-1);
}
int main(int argc, char *argv[]) {
	int i;

	printf("Version %s_%s_%s\n",
			VERSION,  __DATE__, __TIME__);
	parse_options(argc, argv);
	signal(SIGPIPE, SIG_IGN);
	list_init_head(&g_flowi_list);
	for (i = 0; i < MAX_FLOWIS; i++)
		list_init_head(&g_flowi_hlist[i]);
	loop();
	return 0;
}
